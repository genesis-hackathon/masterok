﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using Accord.MachineLearning;
using Accord.Math;
using Accord.Statistics.Analysis;

using Urban.Model;

namespace Urban.Learning
{
    /// <summary>
    /// обучающая модель по типу заявки
    /// </summary>
    public class ServiceLearner
    {
        /// <summary>
        /// количество зданий
        /// </summary>
        private const int BUILDING_COUNT = 8574;

        /// <summary>
        /// коннектор
        /// </summary>
        private readonly Connector conn;

        /// <summary>
        /// тип заявки
        /// </summary>
        private readonly Service service;

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="conn"> коннектор </param>
        /// <param name="service"> тип заявки </param>
        public ServiceLearner(Connector conn, Service service)
        {
            this.conn = conn;
            this.service = service;
        }

        /// <summary>
        /// обучить модель
        /// </summary>
        public void Learn()
        {
            var data = conn.ExecuteReaderEach(@"
select r.d_created::date as date,
       a.id as accident_reason_id,
       r.building_id
  from data.request as r
  join data.lnk_request_accident_reason as lnk on lnk.request_id = r.id
  join data.d_accident_reason as a on a.id = lnk.accident_reason_id
 where r.service_id = 8
   and r.request_status_id = 3
 order by r.id", reader =>
            {
                int i = 0;
                return new AccidentItem()
                {
                    Date = reader.GetDateTime(i++),
                    Reason = reader.GetInt32(i++),
                    Building = reader.GetInt32(i++),
                };
            },
                "P_SERVICE_ID", service.ID
            ).ToList();

            // преобразуем данные в словарь
            var dic = new Dictionary<(DateTime date, int building), AccidentItem>();
            foreach (var item in data)
            {
                dic[(item.Date, item.Building)] = item;
            }

            // входные данные
            var table = new DataTable();
            table.Columns.Add("Temperature", typeof(double));
            table.Columns.Add("Pressure", typeof(double));
            table.Columns.Add("Wind", typeof(double));
            table.Columns.Add("Building", typeof(double));

            // выходные данные
            var outputList = new List<int>();

            // цикл по дням
            for (var date = DateTime.Today.AddDays(-30); date <= DateTime.Today; date = date.AddDays(1))
            {
                // получаем данные о погоде
                var weather = Weather.Get(date);

                for (int building = 1; building < BUILDING_COUNT; building++)
                {
                    // заполняем входные данные
                    table.Rows.Add(weather.Temperature, weather.Pressure, weather.Wind, (double)building * 100.0);

                    // заполняем выходные данные
                    if (dic.TryGetValue((date, building), out var item))
                    {
                        // была авария
                        outputList.Add(1);
                    }
                    else
                    {
                        // не было аварии
                        outputList.Add(0);
                    }
                }
            }

            // обучаем модель
            var model = new KNearestNeighbors(k: 4);
            model.Learn(table.ToJagged(), outputList.ToArray());

            // заполняем аналитику
            using (var tx = conn.BeginTransaction())
            {
                // создаем аналитику
                var decideArray = new double[4];
                for (var date = DateTime.Today.AddDays(15); date >= DateTime.Today; date = date.AddDays(-1))
                {
                    Console.Write('.');

                    // получаем данные о погоде
                    var weather = Weather.Get(date);

                    for (int building = 1; building < BUILDING_COUNT; building++)
                    {
                        decideArray[0] = weather.Temperature;
                        decideArray[1] = weather.Pressure;
                        decideArray[2] = weather.Wind;
                        decideArray[3] = (double)building;

                        // делаем предсказание
                        var result = model.Decide(decideArray);

                        conn.ExecuteNonQuery(@"
insert into data.analytics
(service_id, d_date, building_id, value)
values
(@P_SERVICE_ID, @P_D_DATE, @P_BUILDING_ID, @P_VALUE)",
                            "P_SERVICE_ID", service.ID,
                            "P_D_DATE", date,
                            "P_BUILDING_ID", building,
                            "P_VALUE", (double)result
                        );
                    }

                }
                Console.WriteLine();

                tx.Commit();
            }

        }

        /// <summary>
        /// построить аналитику
        /// </summary>
        public void CreateAnalytics()
        {
            //throw new NotImplementedException();
        }
    }
}

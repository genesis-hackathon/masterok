﻿using System;

namespace Urban.Model
{
    /// <summary>
    /// справочник с кодом
    /// </summary>
    public class DictionaryWithCode : Dictionary, IDictionaryWithCode
    {
        /// <summary>
        /// код
        /// </summary>
        public string Code { get; set; }
    }
}

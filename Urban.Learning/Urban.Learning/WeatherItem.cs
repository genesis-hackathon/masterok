﻿using System;

namespace Urban.Learning
{
    /// <summary>
    /// данные о погоде
    /// </summary>
    public class WeatherItem
    {
        /// <summary>
        /// дата
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// температура
        /// </summary>
        public double Temperature { get; set; }

        /// <summary>
        /// давление
        /// </summary>
        public double Pressure { get; set; }

        /// <summary>
        /// скорость ветра
        /// </summary>
        public double Wind { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Urban.Model;

namespace Urban.DataService.Controllers
{
    /// <summary>
    /// аналитика
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AnalyticsController : ServiceController
    {
        /// <summary>
        /// построить график
        /// </summary>
        /// <param name="start"> начальная дата </param>
        /// <param name="end"> конечная дата </param>
        /// <returns></returns>
        [HttpGet(nameof(GetRequestPrognoz))]
        public ActionResult<ChartData> GetRequestPrognoz(DateTime start, DateTime end)
        {
            var data = new ChartData();

            using (var conn = GetConnector())
            {
                // категории
                for (var date = start; date <= end; date = date.AddDays(1))
                {
                    data.Categories.Add(date.ToString("dd.MM"));
                }

                // серии
                data.Series = conn.ExecuteReaderEach(@"

with dat as (
    select p.name,
           a.d_date as date,
           (sum(a.value) % 25)::int4 as value
      from data.analytics as a
      join data.d_service as s on s.id = a.service_id
      join data.d_service as p on p.id = s.parent_id
     where a.d_date >= @P_START
       and a.d_date <= @P_END
     group by 1,2
     order by 1,2
)
select name,
       array_agg(value order by date)
  from dat as d
 group by 1
 order by name",
                 reader =>
                 {
                     return new Serie
                     {
                         Name = reader.GetString(0),
                         Data = reader.GetInt32Array(1).ToList()
                     };
                 },
                     "P_START", start,
                     "P_END", end
                 ).ToList();
            }

            return data;
        }

        /// <summary>
        /// получить данные статистики
        /// </summary>
        /// <param name="start"> начальная дата </param>
        /// <param name="end"> конечная дата </param>
        /// <returns></returns>
        [HttpGet(nameof(GetStats))]
        public ActionResult<IEnumerable<StatsData>> GetStats(DateTime start, DateTime end)
        {
            using (var conn = GetConnector())
            {
                return conn.ExecuteReaderEach(@"
with dat as (
    select p.id,
           p.name,
           round(extract(epoch from avg(r.d_finished - r.d_started) filter (where r.d_finished is not null)) / 60)::int4 as avg_time_minutes,
           round(extract(epoch from max(r.d_finished - r.d_started) filter (where r.d_finished is not null)) / 60)::int4 as max_time_minutes,
           count(*) filter (where r.d_finished is not null and s.estimated_time * interval '1 hour' >= (r.d_finished - r.d_started)) as cnt_in_time,
           count(*) filter (where r.d_finished is not null and s.estimated_time * interval '1 hour' < (r.d_finished - r.d_started)) as cnt_not_in_time,
           count(*) filter (where true) as request_total,
           count(*) filter (where r.request_status_id = 3) as request_completed,
           count(*) filter (where r.request_status_id = 2) as request_in_progress,
           avg(r.score) as average_score
      from data.request as r
      join data.d_service as s on s.id = r.service_id
      join data.d_service as p on p.id = s.parent_id
     where r.d_created >= @P_START
       and r.d_created <= @P_END
     group by 1,2
     order by 1,2
)
select id,
       name,
       coalesce(avg_time_minutes, 0) as avg_time_minutes,
       coalesce(max_time_minutes, 0) as max_time_minutes,
       (cnt_in_time * 100 / (case when cnt_in_time + cnt_not_in_time = 0 then 1 else cnt_in_time + cnt_not_in_time end)) as percent_in_time,
       coalesce(cnt_not_in_time, 0) as cnt_not_in_time,
       request_total,
       request_completed,
       request_in_progress,
       average_score
  from dat",
                reader =>
                {
                    int i = 0;
                    return new StatsData
                    {
                        ServiceID = reader.GetInt32(i++),
                        ServiceName = reader.GetString(i++),
                        AverageProcessTime = GetProcessTime(reader.GetInt32(i++)),
                        MaxProcessTime = GetProcessTime(reader.GetInt32(i++)),
                        PercentInTime = reader.GetInt32(i++),
                        CountNotInTime = reader.GetInt32(i++),
                        RequestTotal = reader.GetInt32(i++),
                        RequestCompleted = reader.GetInt32(i++),
                        RequestInProgress = reader.GetInt32(i++),
                        AverageScore = reader.GetDouble(i++).ToString("0.00", CultureInfo.InvariantCulture),
                    };
                },
                    "P_START", start,
                    "P_END", end
                ).ToList();
            }
        }

        /// <summary>
        /// форматировать время обработки заявок
        /// </summary>
        /// <param name="value"> количество минут </param>
        /// <returns></returns>
        private string GetProcessTime(int value)
        {
            var sb = new StringBuilder();

            var days = value / (24 * 60);
            var hours = (value % (24 * 60)) / 60;
            var minutes = value % 60;

            if (days > 0)
            {
                sb.Append($"{days}д. ");
            }

            if (days > 0 || hours > 0)
            {
                sb.Append($"{hours}ч. ");
            }

            sb.Append($"{minutes}мин");

            return sb.ToString().Trim();
        }
    }
}

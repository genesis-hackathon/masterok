import Vue from 'vue';

/**
 * проверить, является ли значение строкой
 * @param {any} value значение
 * @return {boolean} результат
 */
export function isString(value) {
  return typeof value === 'string' || value instanceof String;
}

/**
 * проверить, является ли значение функцией
 * @param {any} value значение
 * @return {boolean} результат
 */
export function isFunction(value) {
  return typeof value === 'function';
}

/**
 * проверить, является ли значение объектом
 * @param {any} value значение
 * @return {boolean} результат
 */
export function isObject(value) {
  return value && typeof value === 'object' && value.constructor === Object;
}

/**
 * проверить, является ли значение объектом
 * @param {any} value значение
 * @return {boolean} результат
 */
export function isObjectOrClass(value) {
  // TODO: хрен знает, как это грамотно проверить
  return value && typeof value === 'object';
}

/** конструктор Object */
const ObjectFunctionString = {}.constructor.toString();

/**
 * проверить, что переменная является простым объектом
 * @param {any} value переменная
 * @return {boolean} результат проверки
 */
export function isPlainObject(value) {
  // Detect obvious negatives
  // Use toString instead of jQuery.type to catch host objects
  if (!value || toString.call(value) !== '[object Object]') {
    return false;
  }

  const proto = Object.getPrototypeOf(value);

  // Objects with no prototype (e.g., `Object.create( null )`) are plain
  if (!proto) return true;

  // нельзя копировать реактивные объекты
  if (value.hasOwnProperty('__ob__')) return false;

  // Objects with prototype are plain iff they were constructed by a global Object function
  const Ctor = Object.hasOwnProperty.call(proto, 'constructor') && proto.constructor;
  return typeof Ctor === 'function' && Ctor.toString() === ObjectFunctionString;
}

/**
 * расширить объект из источников
 * @param {Object[]} [sources] источники
 * @return {boolean} результат слияния
 */
export function extend(...sources) {
  let options, name, src, copy, copyIsArray, clone,
    i = 1,
    deep = false;

  const length = sources.length;

  let target = sources[0] || {};

  // Handle a deep copy situation
  if (typeof target === 'boolean') {
    deep = target;

    // Skip the boolean and the target
    target = sources[i] || {};
    i++;
  }

  // Handle case when target is a string or something (possible in deep copy)
  if (typeof target !== 'object' && !isFunction(target)) {
    target = {};
  }

  for (; i < length; i++) {
    // Only deal with non-null/undefined values
    if ((options = sources[i]) != null) {
      // Extend the base object
      for (name in options) {
        if (name) {
          src = target[name];
          copy = options[name];

          // Prevent never-ending loop
          if (target === copy) {
            continue;
          }

          // Recurse if we're merging plain objects or arrays
          if (deep && copy && (isPlainObject(copy) ||
            (copyIsArray = Array.isArray(copy)))) {
            if (copyIsArray) {
              copyIsArray = false;
              clone = src && Array.isArray(src) ? src : [];
            } else {
              clone = src && isPlainObject(src) ? src : {};
            }

            // Never move original objects, clone them
            target[name] = extend(deep, clone, copy);

            // Don't bring in undefined values
          } else if (copy !== undefined) {
            target[name] = copy;
          }
        }
      }
    }
  }

  // Return the modified object
  return target;
}

////////////////////////////////////////////////////////////////
// функции глубокого бурения объектов
////////////////////////////////////////////////////////////////

/**
 * получить компоненты пути до поля
 * @param {String} field имя поля
 * @return {Array} значение
 */
function getDeepFieldArray(field) {
  const arr = [];
  for (let match, matcher = /^([^.[]+)|\.([^.[]+)|\["([^"]+)"\]|\[(\d+)\]/g;
    match = matcher.exec(field);) {
    arr.push(Array.from(match).slice(1).filter((x) => x !== undefined)[0]);
  }
  return arr;
}

/**
 * получить значение поля объекта
 * @param {Object} target целевой объект
 * @param {String} field имя поля
 * @return {any} значение
 */
export function getDeepField(target, field) {
  // извлекаем список ключей
  const arr = getDeepFieldArray(field);

  return arr.reduce((o, f) => {
    return o ? o[f] : o;
  }, target);
}

/**
 * установить значение поля объекта
 * @param {Object} target целевой объект
 * @param {String} field имя поля
 * @param {any} value значение
 * @param {Boolean} reactive указывает, что нужно создавать реактивные объекты
 */
export function setDeepField(target, field, value, reactive = true) {
  // извлекаем список ключей
  const arr = getDeepFieldArray(field);

  // цикл по ключам
  let current = target, sub;
  const n = arr.length;
  for (let i = 0; i < n - 1; i++) {
    sub = arr[i];
    if (isObject(current[sub])) {
      // объект
      current = current[sub];
    } else {
      // что-то другое => переназначаем
      if (reactive) {
        const next = Vue.observable({});
        Vue.set(current, sub, next);
        current = next;
      } else {
        const next = {};
        current[sub] = next;
        current = next;
      }
    }
  }

  // устанавливаем последнее значение
  if (reactive) {
    Vue.set(current, arr[n - 1], value);
  } else {
    current[arr[n - 1]] = value;
  }
}

/**
 * реактивно обновить целевой объект
 * @param {Object} target целевой объект
 * @param {Object} source исходный объект
 */
export function reactiveUpdate(target, source) {
  if (!target || !source) return;

  for (const key of Object.keys(source)) {
    const value = source[key];

    // создаем реактивное свойство при необходимости
    if (!target.hasOwnProperty(key)) Vue.set(target, key, value);

    if (isObject(source[key])) {
      if (isObject(target[key])) {
        // глубокое обновление
        reactiveUpdate(target[key], value);
      } else {
        // целевое свойство не объект
        target[key] = Vue.observable(value);
      }
    } else {
      target[key] = value;
    }
  }
}

/**
 * получить первое поле объекта
 * @param {Object} value объект
 * @return {any} значение
 */
export function firstValue(value) {
  if (value) {
    const keys = Object.keys(value);
    if (keys.length !== 0) {
      return value[keys[0]];
    }
  }
  return undefined;
}

/**
 * установить обработчик изменения размера
 * @param {Function} callback функция
 * @param {number} timeout тайм-аут
 */
export function setResizeHandler(callback, timeout) {
  let timerId = undefined;
  window.addEventListener('resize', function () {
    if (timerId != undefined) {
      clearTimeout(timerId);
      timerId = undefined;
    }
    timerId = setTimeout(function () {
      timerId = undefined;
      callback();
    }, timeout);
  });
}

﻿using System;

namespace Urban.Model
{
    /// <summary>
    /// данные статистики
    /// </summary>
    public class StatsData
    {
        /// <summary>
        /// идентификатор типа заявки
        /// </summary>
        public int ServiceID { get; set; }

        /// <summary>
        /// имя типа заявки
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// среднее время обработки заявки
        /// </summary>
        public string AverageProcessTime { get; set; }

        /// <summary>
        /// максимальное время обработки заявки
        /// </summary>
        public string MaxProcessTime { get; set; }

        /// <summary>
        /// процент выполненный вовремя заявок
        /// </summary>
        public int PercentInTime { get; set; }

        /// <summary>
        /// количество не выполненных вовремя заявок
        /// </summary>
        public int CountNotInTime { get; set; }

        /// <summary>
        /// заявок получено
        /// </summary>
        public int RequestTotal { get; set; }

        /// <summary>
        /// заявок выполнено
        /// </summary>
        public int RequestCompleted { get; set; }

        /// <summary>
        /// заявок в работе
        /// </summary>
        public int RequestInProgress { get; set; }

        /// <summary>
        /// средняя оценка заявки
        /// </summary>
        public string AverageScore { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace Urban.Model
{
    /// <summary>
    /// строение на карте
    /// </summary>
    public class MapBuilding
    {
        /// <summary>
        /// идентификатор
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// тип улицы
        /// </summary>
        public string StreetType { get; set; }

        /// <summary>
        /// имя улицы
        /// </summary>
        public string StreetName { get; set; }

        /// <summary>
        /// номер дома
        /// </summary>
        public string HouseNumber { get; set; }

        /// <summary>
        /// количество этажей
        /// </summary>
        public int Floors { get; set; }

        /// <summary>
        /// местоположение
        /// </summary>
        public GeoPoint Location { get; set; }

        /// <summary>
        /// геометрия
        /// </summary>
        public string Geometry { get; set; }
    }
}

﻿using System;

namespace Urban.Model
{
    /// <summary>
    /// базовый класс заявки
    /// </summary>
    public abstract class RequestBase
    {
        /// <summary>
        /// идентификатор заявки
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// идентификатор типа заявки
        /// </summary>
        public int ServiceID { get; set; }

        /// <summary>
        /// Идентификатор статуса заявки
        /// </summary>
        public int RequestStatusID { get; set; }

        /// <summary>
        /// Идентификатор автора
        /// </summary>
        public int AuthorID { get; set; }

        /// <summary>
        /// Дата создания заявки 
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Дата взятия в работу
        /// </summary>
        public DateTime? DateStarted { get; set; }

        /// <summary>
        /// Дата завершения заявки
        /// </summary>
        public DateTime? DateFinished { get; set; }

        /// <summary>
        /// идентификатор здания
        /// </summary>
        public int? BuildingID { get; set; }

        /// <summary>
        /// местоположение
        /// </summary>
        public GeoPoint Location { get; set; }

        /// <summary>
        /// описание заявки
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Идентификатор оригинала заявки при дубле
        /// </summary>
        public int? DuplicateRequestID { get; set; }

        /// <summary>
        /// Идентификатор причины отклонения заявки
        /// </summary>
        public int? RejectReasonID { get; set; }

        /// <summary>
        /// Комментарий после закрытия заявки
        /// </summary>
        public string Comment { get; set; }

        /// <summary>
        /// Идентификатор исполнителя
        /// </summary>
        public int? PerformerID { get; set; }

        ////////////////////////////////////////////////////////////////
        // комплексные поля
        ////////////////////////////////////////////////////////////////

        /// <summary>
        /// идентификаторы файлов
        /// </summary>
        public int[] Files { get; set; }

        /// <summary>
        /// причины аварии
        /// </summary>
        public int[] AccidentReasons { get; set; }
    }
}
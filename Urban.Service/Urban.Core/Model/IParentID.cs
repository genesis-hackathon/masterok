﻿using System;

namespace Urban.Model
{
    /// <summary>
    /// интерфейс справочника
    /// </summary>
    public interface IParentID
    {
        /// <summary>
        /// идентификатор родителя
        /// </summary>
        int? ParentID { get; set; }
    }
}

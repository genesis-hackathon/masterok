import axios from 'axios';
import { BASE_URL } from '@/utils/constants.js'

const API = {
    getServices() {
        return axios.get(`${BASE_URL}Dictionary/Service`)
            .then(res => res.data)
            .catch((err) => {
                throw new Error(err);
            });
    },

    getRequest(params) {
        return axios.get(`${BASE_URL}Request`, { params })
            .catch((err) => {
                throw new Error(err);
            });
    },

    saveRequest(params) {
        return axios.post(`${BASE_URL}Request`, { ...params })
            .catch(err => {
                throw new Error(err);
            });
    },

    uploadImg(fd, params) {
        return axios({
            url: `${BASE_URL}File${params}`,
            method: 'post',
            data: fd,
            processData: false,
            contentType: false
        });
    },

    getDict(params) {
        console.log(params);
        return axios.get(`${BASE_URL}Dictionary/AccidentReason`, { params })
            .catch((err) => {
                throw new Error(err);
            });
    }
};

export default API;

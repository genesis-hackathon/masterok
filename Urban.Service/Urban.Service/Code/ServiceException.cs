﻿using System;
using System.Net;

namespace Urban.DataService
{
    /// <summary>
    /// исключение сервиса
    /// </summary>
    public class ServiceException : Exception
    {
        /// <summary>
        /// код ошибки
        /// </summary>
        public HttpStatusCode Code { get; set; } = HttpStatusCode.BadRequest;

        /// <summary>
        /// конструктор
        /// </summary>
        public ServiceException() { }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="message"> сообщение </param>
        public ServiceException(string message) : base(message) { }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="message"> сообщение </param>
        /// <param name="innerException"> внутреннее исключение </param>
        public ServiceException(string message, Exception innerException) : base(message, innerException) { }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="code"> код ошибки </param>
        public ServiceException(HttpStatusCode code)
        {
            this.Code = code;
        }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="message"> сообщение </param>
        /// <param name="code"> код ошибки </param>
        public ServiceException(string message, HttpStatusCode code) : base(message)
        {
            this.Code = code;
        }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="message"> сообщение </param>
        /// <param name="innerException"> внутреннее исключение </param>
        /// <param name="code"> код ошибки </param>
        public ServiceException(string message, Exception innerException, HttpStatusCode code) : base(message, innerException)
        {
            this.Code = code;
        }
    }
}

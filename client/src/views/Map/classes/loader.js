import { Feature } from 'ol';
import { Circle as CircleStyle, Fill, Stroke, Style } from 'ol/style.js';
import { Polygon } from 'ol/geom.js';
import { transform, transformExtent, fromLonLat } from 'ol/proj.js';

import axios from 'axios';
import { BASE_URL } from '@/utils/constants.js'

/**
 * преобразовать геометрию в полигон
 * @param {any} geometry геометрия
 * @param {any} source исходная проекция
 * @param {any} target целевая проекция
 */
function transformGeometry(geometry, source, target) {
  geometry = JSON.parse(geometry);
  const arr = [];
  geometry.coordinates.forEach((sub) => {
    const subArr = [];
    sub.forEach((p) => {
      subArr.push(transform(p, 'EPSG:4326', 'EPSG:900913'));
    })
    arr.push(subArr);
  });
  return arr;
}

/**
 * получить отображаемое имя фичи
 * @param {any} item фича
 */
function getFeatureName(item) {
  let res = '';
  if (item.StreetType != '-') res = res + `${item.StreetType}. `;
  res += item.StreetName;
  res += ', ';
  res += item.HouseNumber;
  return res;
}

/**
 * загрузчик
 * @param {any} core ядро
 * @param {any} vectorSource векторный слой
 * @param {any} extent область покрытия
 * @param {any} resolution разрешение
 * @param {any} projection проекция
 */
export function LayerLoader(core, vectorSource, extent, resolution, projection) {
  const bbox = transformExtent(extent, 'EPSG:900913', 'EPSG:4326');
  const proj = projection.getCode();
  const url = `${BASE_URL}Map/GetFeatures?bbox=${bbox}`;

  const options = core.$options;
  const analytics = core.$analytics;

  axios.get(url)
    .then((res) => {
      // получение списка домов
      res.data.forEach((item) => {
        // удаляем предыдущую фичу
        const old = vectorSource.getFeatureById(item.ID);
        if (old) vectorSource.removeFeature(old);

        const geometryData = transformGeometry(item.Geometry, 'EPSG:4326', proj);
        const geometry = new Polygon(geometryData);
        const feature = new Feature({
          buildingID: item.ID,
          location: [item.Location.Lon, item.Location.Lat],
          name: getFeatureName(item),
          geometry: geometry
        });
        feature.setId(item.ID);
        vectorSource.addFeature(feature);
      });

      if (options.mode == 'analytics') {
        core.updateFeatureAnalytics();
      }
    })
    .catch((err) => {
      vectorSource.removeLoadedExtent(extent);
      throw new Error(err);
    });
};

﻿using System;

namespace Urban.Model
{
    /// <summary>
    /// точка на карте
    /// </summary>
    [System.Diagnostics.DebuggerDisplay("({Lon};{Lat})")]
    public class GeoPoint
    {
        /// <summary>
        /// долгота
        /// </summary>
        public double Lon { get; set; }

        /// <summary>
        /// широта
        /// </summary>
        public double Lat { get; set; }

        /// <summary>
        /// конструктор
        /// </summary>
        public GeoPoint() { }

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="array"> массив </param>
        public GeoPoint(double[] array)
        {
            Lon = array[0];
            Lat = array[1];
        }

        /// <summary>
        /// преобразовать в массив
        /// </summary>
        /// <returns></returns>
        public double[] ToArray()
        {
            return new double[] { Lon, Lat };
        }
    }
}
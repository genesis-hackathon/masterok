import ProxyRouter from '@/components/ProxyRouter.vue';

export default [
    {
        path: '/',
        name: 'home',
        redirect: { name: 'user' }
    }, {
        path: '/user',
        name: 'user',
        redirect: { name: 'user-categories' },
        component: () => import('@/views/User/User.vue'),
        children: [
            {
                path: 'categories',
                component: ProxyRouter,
                children: [
                    {
                        path: '',
                        name: 'user-categories',
                        component: () => import('@/views/User/UserCategories.vue'),
                    }, {
                        path: ':id',
                        name: 'user-create',
                        component: () => import('@/views/User/UserCreate.vue')
                    }
                ]
            }, {
                path: 'list',
                component: ProxyRouter,
                children: [
                    {
                        path: '',
                        name: 'user-list',
                        component: () => import('@/views/User/UserList.vue'),
                    }, {
                        path: ':id',
                        name: 'user-request',
                        component: () => import('@/views/User/UserRequest.vue'),
                    }
                ]
            }, {
                path: 'geo',
                name: 'user-map',
                component: () => import('@/views/User/UserGeo.vue')
            }
            , {
                path: 'dashboard',
                name: 'user-dashboard',
                component: () => import('@/views/User/UserDashboard.vue')
            }
        ]
    }, {
        path: '/uk',
        name: 'uk',
        component: () => import('@/views/Uk/Uk.vue'),
        redirect: {name: 'uk-list'},
        children: [
            {
                path: 'list',
                component: ProxyRouter,
                children: [
                    {
                        path: '',
                        name: 'uk-list',
                        component: () => import('@/views/Uk/UkList.vue'),
                    }, {
                        path: ':id',
                        name: 'uk-request',
                        component: () => import('@/views/Uk/UkRequest.vue'),
                    }
                ]
            }
        ]
    }, {
        path: '/analytics',
        name: 'analytics',
        component: () => import('@/views/Analytics/Analytics.vue')
    }

];

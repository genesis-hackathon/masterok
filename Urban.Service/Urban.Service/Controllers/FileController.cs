﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Urban.Model;

namespace Urban.DataService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ServiceController
    {
        /// <summary>
        /// путь к файлам
        /// </summary>
        private readonly string filePath;

        /// <summary>
        /// типы MIME
        /// </summary>
        private static readonly Dictionary<string, string> _mimeTypes = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase)
        {
            {".txt", "text/plain"},
            {".pdf", "application/pdf"},
            {".doc", "application/vnd.ms-word"},
            {".docx", "application/vnd.ms-word"},
            {".xls", "application/vnd.ms-excel"},
            {".xlsx", "officedocument.spreadsheetml.sheet"},
            {".png", "image/png"},
            {".jpg", "image/jpeg"},
            {".jpeg", "image/jpeg"},
            {".gif", "image/gif"},
            {".csv", "text/csv"}
        };

        /// <summary>
        /// конструктор
        /// </summary>
        public FileController(IHostingEnvironment environment)
        {
            filePath = Path.Combine(environment.ContentRootPath, "uploads");
            if (!Directory.Exists(filePath)) Directory.CreateDirectory(filePath);
        }

        /// <summary>
        /// получить файл
        /// </summary>
        /// <param name="fileID"> идентификатор файла </param>
        /// <returns></returns>
        [HttpGet("{fileID}")]
        public IActionResult Get(int fileID)
        {
            using (var conn = GetConnector())
            {
                string filename = default, file = default;

                conn.ExecuteReaderEach(@"
select coalesce(body, '') as filename,
       file
  from data.request_file
 where id = @P_FILE_ID
",
                    reader =>
                    {
                        filename = reader.GetString(0);
                        file = reader.GetString(1);
                    },
                    "P_FILE_ID", fileID
                );

                if (string.IsNullOrWhiteSpace(filename))
                {
                    return NoContent();
                }
                else
                {
                    var memory = new MemoryStream();
                    using (var stream = new FileStream(filename, FileMode.Open))
                    {
                        stream.CopyTo(memory);
                    }
                    memory.Position = 0;
                    return File(memory, GetContentType(file), Path.GetFileName(file));
                }
            }
        }

        /// <summary>
        /// отправить файл
        /// </summary>
        /// <param name="requestID"> идентификатор заявки </param>
        /// <param name="authorID"> идентификатор автора </param>
        /// <param name="isAfter"> признак, что файл отправлен после завершения заявки </param>
        /// <param name="files"> список файлов </param>
        [HttpPost]
        public ActionResult<int> Post(
            [FromQuery] int requestID,
            [FromQuery] int authorID,
            [FromQuery] bool isAfter,
            [FromForm(Name = "file")] IFormFileCollection files)
        {
            if (files != default && files.Count != 0)
            {
                using (var conn = GetConnector())
                {
                    using (var tx = conn.BeginTransaction())
                    {
                        // получаем количество файлов в заявке
                        int order = conn.ExecuteScalar<int>(@"
select coalesce(max(ord), 0) from data.request_file where request_id = @P_REQUEST_ID",
                            "P_REQUEST_ID", requestID);

                        foreach (var file in files)
                        {
                            // сохраняем файл в базе
                            var fileID = conn.ExecuteScalar<int>(@"
insert into data.request_file
(request_id, file, body, author_user_id, ord, is_after)
values
(@P_REQUEST_ID, @P_FILE, @P_BODY, @P_AUTHOR_USER_ID, @P_ORD, @P_IS_AFTER)
returning id",
                                "P_REQUEST_ID", requestID,
                                "P_FILE", file.FileName,
                                "P_BODY", "filename",
                                "P_AUTHOR_USER_ID", authorID,
                                "P_ORD", ++order,
                                "P_IS_AFTER", isAfter
                            );

                            // сохраняем файл в системе
                            var filename = GetFileName(fileID);

                            using (var stream = new FileStream(filename, FileMode.Create))
                            {
                                file.CopyTo(stream);
                            }

                            // обновляем имя файла в БД
                            conn.ExecuteNonQuery(@"
update data.request_file
   set body = @P_FILENAME
 where id = @P_ID",
                                "P_ID", fileID,
                                "P_FILENAME", filename
                            );
                        }

                        tx.Commit();
                    }
                }

                return files.Count;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// получить MIME-тип
        /// </summary>
        /// <param name="filename"> имя файла </param>
        /// <returns></returns>
        private string GetContentType(string filename)
        {
            return _mimeTypes.TryGetValue(Path.GetExtension(filename).ToLowerInvariant(), out var value) ?
                value :
                "application/octet-stream";
        }

        //// PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}

        /// <summary>
        /// получить имя файла
        /// </summary>
        /// <param name="id"> идентификатор </param>
        /// <returns></returns>
        private string GetFileName(int id)
        {
            return Path.Combine(filePath, $"file_{id}");
        }
    }
}

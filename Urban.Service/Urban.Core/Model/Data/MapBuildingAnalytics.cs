﻿using System;

namespace Urban.Model
{
    /// <summary>
    /// аналитика на карте
    /// </summary>
    public class MapBuildingAnalytics
    {
        /// <summary>
        /// идентификатор здания
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// значение
        /// </summary>
        public double Value { get; set; }
    }
}

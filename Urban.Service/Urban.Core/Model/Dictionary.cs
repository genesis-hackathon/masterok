﻿using System;

namespace Urban.Model
{
    /// <summary>
    /// справочник
    /// </summary>
    [System.Diagnostics.DebuggerDisplay("{ID}: {Name}")]
    public class Dictionary : IDictionary
    {
        /// <summary>
        /// идентификатор
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// наименование
        /// </summary>
        public string Name { get; set; }
    }
}

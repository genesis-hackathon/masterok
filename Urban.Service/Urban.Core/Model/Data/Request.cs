﻿using System;

namespace Urban.Model
{
    /// <summary>
    /// полная информация о заявке
    /// </summary>
    public class Request : RequestBase
    {
        /// <summary>
        /// адрес
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// имя типа заявки
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// имя типа статуса
        /// </summary>
        public string RequestStatusName { get; set; }
    }
}

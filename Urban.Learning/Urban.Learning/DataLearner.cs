﻿using System;
using System.Collections.Generic;
using System.Linq;

using Urban.Model;

namespace Urban.Learning
{
    /// <summary>
    /// обучающий сервис
    /// </summary>
    public class DataLearner : IDisposable
    {
        /// <summary>
        /// коннектор
        /// </summary>
        private readonly Connector conn;

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="conn"> коннектор </param>
        public DataLearner(Connector conn)
        {
            this.conn = conn;
        }

        /// <summary>
        /// уничтожить сущность
        /// </summary>
        public void Dispose()
        {
            if (conn != default)
            {
                conn.Dispose();
            }
        }

        /// <summary>
        /// запустить
        /// </summary>
        public void Run()
        {
            // читаем прогноз погоды
            Weather.Load(conn);

            // цикл по типам заявок
            foreach (var service in conn.ExecuteReaderEach(@"
select id, name
  from data.d_service
 where parent_id is not null
 order by 1
",
                reader =>
                {
                    return new Service
                    {
                        ID = reader.GetInt32(0),
                        Name = reader.GetString(1)
                    };
                }).ToList())
            {
                // обучаем модель для данного типа заявки
                var serviceLearner = new ServiceLearner(conn, service);

                // обучаем модель
                serviceLearner.Learn();
            }
        }
    }
}

import 'element-ui/lib/theme-chalk/index.css';

import Vue from 'vue';
import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/ru-RU';

import router from '@/router';
import store from '@/store';
import App from '@/views/App.vue';
import '@/styles/index.css';

Vue.config.productionTip = false;
Vue.use(ElementUI, { locale });

window.vm = new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');

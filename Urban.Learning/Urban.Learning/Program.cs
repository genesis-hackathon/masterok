﻿using System;
using System.IO;
using System.Linq;

namespace Urban.Learning
{
    public static class Program
    {
        /// <summary>
        /// строка соединения с БД
        /// </summary>
        public static string ConnectionString { get; private set; }

        /// <summary>
        /// получить коннектор
        /// </summary>
        /// <returns></returns>
        public static Connector GetConnector()
        {
            var conn = new Connector(ConnectionString);
            conn.Open();
            return conn;
        }

        /// <summary>
        /// точка входа
        /// </summary>
        static void Main()
        {
            Program.ConnectionString = File.ReadAllLines(Path.Combine(AppContext.BaseDirectory, "config.txt"))
                .Where(e => !string.IsNullOrWhiteSpace(e))
                .First();

            new DataLearner(GetConnector()).Run();
        }
    }
}

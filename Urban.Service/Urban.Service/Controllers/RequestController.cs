﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Urban.Model;

namespace Urban.DataService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RequestController : ServiceController
    {
        /// <summary>
        /// получить список заявок
        /// </summary>
        /// <param name="userID"> идентификатор автора </param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<IEnumerable<Request>> Get(int userID, int? limit)
        {
            using (var conn = GetConnector())
            {
                var sql = @"
select r.id,
       r.service_id,
       r.request_status_id,
       r.author_user_id,
       r.d_created,
       r.d_started,
       r.d_finished,
       r.building_id,
       r.location,
       r.description,
       r.duplicate_request_id,
       r.reject_reason_id,
       r.comment,
       r.performer_user_id,
       -- список файлов
       array(select id from data.request_file as e where e.request_id = r.id) as files,
       -- причины аварии
       array(select accident_reason_id from data.lnk_request_accident_reason as e where e.request_id = r.id) as accident_reasons,
       -- адрес
       case
         when r.building_id is not null then (select map.get_building_address(r.building_id))
         else (
            select map.get_building_address(b.id)
              from map.building as b
             order by ((b.center[1] - r.location[1]) * (b.center[1] - r.location[1]) + (b.center[2] - r.location[2]) * (b.center[2] - r.location[2])) desc
             limit 1
         )
       end as address,
       s.name as service_name,
       t.name as status_name
  from data.request as r
  join data.d_service as s on s.id = r.service_id
  join data.d_request_status as t on t.id = r.request_status_id
 where r.author_user_id = @P_USER_ID
   and r.author_user_id != 2
 order by d_created desc
";

                if (limit.HasValue)
                {
                    sql += $@"
  limit {limit.Value}";
                }

                return conn.ExecuteReaderEach(sql, reader =>
                {
                    int i = 0;
                    return new Request
                    {
                        ID = reader.GetInt32(i++),
                        ServiceID = reader.GetInt32(i++),
                        RequestStatusID = reader.GetInt32(i++),
                        AuthorID = reader.GetInt32(i++),
                        DateCreated = reader.GetDateTime(i++),
                        DateStarted = reader.GetDateTimeOrNull(i++),
                        DateFinished = reader.GetDateTimeOrNull(i++),
                        BuildingID = reader.GetInt32OrNull(i++),
                        Location = new GeoPoint(reader.GetDoubleArray(i++)),
                        Description = reader.GetStringOrDefault(i++),
                        DuplicateRequestID = reader.GetInt32OrNull(i++),
                        RejectReasonID = reader.GetInt32OrNull(i++),
                        Comment = reader.GetStringOrDefault(i++),
                        PerformerID = reader.GetInt32OrNull(i++),
                        Files = reader.GetInt32Array(i++),
                        AccidentReasons = reader.GetInt32Array(i++),

                        // дополнительные поля
                        Address = reader.GetString(i++),
                        ServiceName = reader.GetString(i++),
                        RequestStatusName = reader.GetString(i++),
                    };
                },
                    "P_USER_ID", userID
                ).ToList();
            }
        }

        //// GET api/values/5
        //[HttpGet("{id}")]
        //public ActionResult<string> Get(int id)
        //{
        //    return "value";
        //}

        /// <summary>
        /// отправить заявку
        /// </summary>
        /// <param name="request"> заявка </param>
        [HttpPost]
        public ActionResult<int> Post([FromBody] CreateRequest request)
        {
            using (var conn = GetConnector())
            {
                var res = conn.ExecuteScalar<int>(@"
insert into data.request
(service_id, author_user_id, building_id, location, description)
values
(@P_SERVICE_ID, @P_AUTHOR_USER_ID, @P_BUILDING_ID, @P_LOCATION, @P_DESCRIPTION)
returning id",
                     "P_SERVICE_ID", request.ServiceID,
                     "P_AUTHOR_USER_ID", request.AuthorID,
                     "P_BUILDING_ID", request.BuildingID,
                     "P_LOCATION", request.Location.ToArray(),
                     "P_DESCRIPTION", request.Description
                 );
                return res;
            }
        }

        //// PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE api/values/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}

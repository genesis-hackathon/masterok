module.exports = {
    plugins: {
        autoprefixer: {},
        'postcss-simple-vars': {},
        'postcss-nested': {},
        'postcss-custom-properties': {},
        'postcss-calc': {},
        'postcss-preset-env': {
            stage: 0,
            features: {
                'color-mod-function': { unresolved: 'warn' }
            },
            importFrom: [
                // './src/styles/theme.pcss'
            ],
            browsers: [
                '> 1%',
                'last 2 versions',
                'not ie <= 8'
            ]
        }
    }
};

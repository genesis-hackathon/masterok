﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace Urban.Model
{
    /// <summary>
    /// данные для графика
    /// </summary>
    public class ChartData
    {
        /// <summary>
        /// категории
        /// </summary>
        [JsonProperty("categories")]
        public List<string> Categories { get; set; }

        /// <summary>
        /// серии
        /// </summary>
        [JsonProperty("series")]
        public List<Serie> Series { get; set; }

        /// <summary>
        /// конструктор
        /// </summary>
        public ChartData()
        {
            Categories = new List<string>();
            Series = new List<Serie>();
        }
    }
}

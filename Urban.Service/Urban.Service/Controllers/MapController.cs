﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Urban.Model;

namespace Urban.DataService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MapController : ServiceController
    {
        /// <summary>
        /// получить фичи в области
        /// </summary>
        /// <returns></returns>
        [HttpGet(nameof(GetFeatures))]
        public ActionResult<IEnumerable<MapBuilding>> GetFeatures(string bbox)
        {
            var bounds = bbox.Split(",").Select(e => double.Parse(e, NumberStyles.Any, CultureInfo.InvariantCulture)).ToArray();

            using (var conn = GetConnector())
            {
                return conn.ExecuteReaderEach(@"
-- получение списка домов
select b.id,
       abbr.abbreviation as street_type,
       s.name as street_name,
       h.number as house_number,
       coalesce(b.floors, 1) as floors,
       b.center,
       b.geometry::text
  from map.building as b
  join map.house as h on h.id = b.house_id
  join map.street as s on s.id = h.street_id
  join map.d_address_abbreviation as abbr on abbr.id = s.address_abbreviation_id
 where b.center[1] >= @P_B0
   and b.center[2] >= @P_B1
   and b.center[1] <= @P_B2
   and b.center[2] <= @P_B3
 order by street_name, house_number", reader =>
                {
                    int i = 0;
                    return new MapBuilding()
                    {
                        ID = reader.GetInt32(i++),
                        StreetType = reader.GetString(i++),
                        StreetName = reader.GetString(i++),
                        HouseNumber = reader.GetString(i++),
                        Floors = reader.GetInt32(i++),
                        Location = new GeoPoint(reader.GetDoubleArray(i++)),
                        Geometry = reader.GetString(i++),
                    };
                },
                    "P_B0", bounds[0],
                    "P_B1", bounds[1],
                    "P_B2", bounds[2],
                    "P_B3", bounds[3]
                ).ToList();
            }
        }

        /// <summary>
        /// построить аналитику
        /// </summary>
        /// <param name="serviceID"> идентификатор типа заявки </param>
        /// <param name="start"> начальная дата </param>
        /// <param name="end"> конечная дата </param>
        /// <returns></returns>
        [HttpGet(nameof(GetAnalytics))]
        public ActionResult<IEnumerable<MapBuildingAnalytics>> GetAnalytics(int serviceID, DateTime start, DateTime end)
        {
            using (var conn = GetConnector())
            {
                return conn.ExecuteReaderEach(@"
select a.building_id,
       avg(a.value) as value
  from data.analytics as a
 where a.service_id = @P_SERVICE_ID
   and a.d_date >= @P_START
   and a.d_date <= @P_END
 group by 1",
                reader =>
                {
                    return new MapBuildingAnalytics
                    {
                        ID = reader.GetInt32(0),
                        Value = reader.GetDouble(1)
                    };
                },
                    "P_SERVICE_ID", serviceID,
                    "P_START", start,
                    "P_END", end
                ).ToList();
            }
        }
    }
}

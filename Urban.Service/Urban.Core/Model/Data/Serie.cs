﻿using System;
using System.Collections.Generic;

using Newtonsoft.Json;

namespace Urban.Model
{
    /// <summary>
    /// серия
    /// </summary>
    public class Serie
    {
        /// <summary>
        /// имя серии
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// значения
        /// </summary>
        [JsonProperty("data")]
        public List<int> Data { get; set; }

        /// <summary>
        /// конструктор
        /// </summary>
        public Serie()
        {
            Data = new List<int>();
        }
    }
}
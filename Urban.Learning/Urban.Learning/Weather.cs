﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Urban.Learning
{
    /// <summary>
    /// данные о погоде
    /// </summary>
    public static class Weather
    {
        /// <summary>
        /// погодные данные
        /// </summary>
        private static Dictionary<DateTime, WeatherItem> _data;

        /// <summary>
        /// загрузить данные погоды
        /// </summary>
        /// <param name="conn"> коннектор </param>
        public static void Load(Connector conn)
        {
            var data = conn.ExecuteReaderEach(@"
select date,
       temperature,
       pressure,
       wind_speed
  from data.weather", reader =>
            {
                int i = 0;
                return new WeatherItem
                {
                    Date = reader.GetDateTime(i++),
                    Temperature = reader.GetDouble(i++),
                    Pressure = reader.GetDouble(i++),
                    Wind = reader.GetDouble(i++),
                };
            }).ToList();

            _data = data.ToDictionary(e => e.Date);
        }

        /// <summary>
        /// получить данные о погоде на дату
        /// </summary>
        /// <param name="date"> дата </param>
        /// <returns></returns>
        public static WeatherItem Get(DateTime date)
        {
            return _data[date.Date];
        }
    }
}
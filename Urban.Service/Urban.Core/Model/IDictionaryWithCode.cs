﻿using System;

namespace Urban.Model
{
    /// <summary>
    /// интерфейс справочника с кодом
    /// </summary>
    public interface IDictionaryWithCode : IDictionary
    {
        /// <summary>
        /// код
        /// </summary>
        string Code { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Urban.Model;

using READER = Npgsql.NpgsqlDataReader;

namespace Urban.DataService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DictionaryController : ServiceController
    {
        /// <summary>
        /// справочник причин отклонения
        /// </summary>
        /// <param name="serviceID"> идентификатор типа заявки </param>
        /// <returns></returns>
        [HttpGet(nameof(AccidentReason))]
        public IEnumerable<AccidentReason> AccidentReason(int serviceID)
        {
            using (var conn = GetConnector())
            {
                return ExecuteDictionaryWithCodeReader<AccidentReason>(conn, @"
select r.id,
       r.name,
       r.code
  from data.d_accident_reason as r
  join data.lnk_service_accident_reason as lnk on lnk.accident_reason_id = r.id
 where lnk.service_id = @P_SERVICE_ID
 order by 1",
                    default,
                    "P_SERVICE_ID", serviceID
                );
            }
        }

        /// <summary>
        /// справочник типов заявок
        /// </summary>
        /// <returns></returns>
        [HttpGet(nameof(Service))]
        public IEnumerable<Service> Service()
        {
            using (var conn = GetConnector())
            {
                return ExecuteDictionaryWithCodeReader<Service>(conn, @"
with recursive dat as (
    select id,
           name,
           code,
           parent_id,
           array[id]::int4[] as hier
      from data.d_service
     where parent_id is null
     --
     union all
     --
    select e.id,
           e.name,
           e.code,
           e.parent_id,
           d.hier || e.parent_id
      from data.d_service as e
      join dat as d on d.id = e.parent_id
)
select id,
       name,
       code,
       parent_id
  from dat
 order by hier, name",
                (item, reader, i) =>
                {
                    item.ParentID = reader.GetInt32OrNull(i++);
                });
            }
        }

        /// <summary>
        /// справочник причин отклонения
        /// </summary>
        /// <returns></returns>
        [HttpGet(nameof(RejectReason))]
        public IEnumerable<RejectReason> RejectReason()
        {
            using (var conn = GetConnector())
            {
                return ExecuteDictionaryWithCodeReader<RejectReason>(conn, @"
select id,
       name,
       code
  from data.d_reject_reason
 order by 1");
            }
        }

        /// <summary>
        /// справочник статусов заявок
        /// </summary>
        /// <returns></returns>
        [HttpGet(nameof(RequestStatus))]
        public IEnumerable<RequestStatus> RequestStatus()
        {
            using (var conn = GetConnector())
            {
                return ExecuteDictionaryWithCodeReader<RequestStatus>(conn, @"
select id,
       name,
       code
  from data.d_request_status
 order by 1");
            }
        }

        /// <summary>
        /// извлечь справочник
        /// </summary>
        /// <typeparam name="T"> тип элемента </typeparam>
        /// <param name="conn"> коннектор </param>
        /// <param name="sql"> запрос </param>
        /// <param name="act"> действие </param>
        /// <returns></returns>
        private IEnumerable<T> ExecuteDictionaryWithCodeReader<T>(Connector conn, string sql,
            Action<T, READER, int> act = default,
            params object[] args)
            where T : IDictionaryWithCode, new()
        {
            return conn.ExecuteReaderEach(sql, reader =>
            {
                int i = 0;

                var item = new T()
                {
                    ID = reader.GetInt32(i++),
                    Name = reader.GetString(i++),
                    Code = reader.GetString(i++),
                };

                if (act != default)
                {
                    // выполняем дополнительную обработку
                    act(item, reader, i);
                }

                return item;
            }, args).ToList();
        }
    }
}

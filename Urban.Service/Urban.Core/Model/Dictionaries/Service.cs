﻿using System;

namespace Urban.Model
{
    /// <summary>
    /// тип заявки
    /// </summary>
    public class Service : DictionaryWithCode, IParentID
    {
        /// <summary>
        /// идентификатор родителя
        /// </summary>
        public int? ParentID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Urban.DataService.Controllers
{
    public abstract class ServiceController : Controller
    {
        /// <summary>
        /// коннектор по умолчанию
        /// </summary>
        protected static Connector _defaultConnector;

        protected static object _lock = new object();

        /// <summary>
        /// получить коннектор БД
        /// </summary>
        /// <returns></returns>
        protected Connector GetConnector()
        {
            var res = new Connector(Program.Configuration["ConnectionStrings:DB"]);

            res.Open();

            return res;
        }

        /// <summary>
        /// получить коннектор БД по умолчанию
        /// </summary>
        /// <returns></returns>
        protected Connector GetDefaultConnector()
        {
            lock (_lock)
            {
                if (_defaultConnector == null) _defaultConnector = new Connector();

                var res = _defaultConnector;

                if (res.Connection == null)
                {
                    res.Open();
                }
                else
                {
                    switch (res.Connection.State)
                    {
                        case System.Data.ConnectionState.Broken:
                        case System.Data.ConnectionState.Closed:
                            res.Open();
                            break;
                    }
                }

                return res;
            }
        }

        /// <summary>
        /// получить числовой идентификатор
        /// </summary>
        /// <param name="id"> строковый идентификатор </param>
        /// <returns></returns>
        protected int GetIdentifier(string id)
        {
            if (!int.TryParse(id, out int intId))
            {
                throw CreateFaultException($"Некорректный идентификатор: {id}");
            }
            return intId;
        }

        /// <summary>
        /// создать исключение сервиса
        /// </summary>
        /// <param name="message"> сообщение </param>
        /// <returns></returns>
        protected Exception CreateFaultException(string message)
        {
            return new ServiceException(message);
        }

        /// <summary>
        /// создать исключение сервиса
        /// </summary>
        /// <param name="message"> сообщение </param>
        /// <param name="code"> код ошибки </param>
        /// <returns></returns>
        protected Exception CreateFaultException(string message, HttpStatusCode code)
        {
            return new ServiceException(message, code);
        }
    }

}

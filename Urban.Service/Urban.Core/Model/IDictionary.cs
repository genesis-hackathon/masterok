﻿using System;

namespace Urban.Model
{
    /// <summary>
    /// интерфейс справочника
    /// </summary>
    public interface IDictionary
    {
        /// <summary>
        /// идентификатор
        /// </summary>
        int ID { get; set; }

        /// <summary>
        /// наименование
        /// </summary>
        string Name { get; set; }
    }
}

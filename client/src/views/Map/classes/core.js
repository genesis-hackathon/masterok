import 'ol/ol.css';
import Feature from 'ol/Feature.js';
import Map from 'ol/Map.js';
import View from 'ol/View.js';
import GeoJSON from 'ol/format/GeoJSON.js';
import Circle from 'ol/geom/Circle.js';
import Point from 'ol/geom/Point.js';
import { Tile as TileLayer, Vector as VectorLayer } from 'ol/layer.js';
import { OSM, Vector as VectorSource } from 'ol/source.js';
import { transform, transformExtent, fromLonLat } from 'ol/proj.js';
import { Circle as CircleStyle, Fill, Stroke, Style } from 'ol/style.js';
import { bbox } from 'ol/loadingstrategy';

import { Options } from '..';
import { LayerLoader } from './loader';

import { setResizeHandler, extend } from '../helpers/utils';

import axios from 'axios';
import { BASE_URL } from '@/utils/constants.js'

/**
 * ядро системы
 */
export default class Core {
  /**
   * конструктор
   * @param {Object} options настройки
   */
  constructor(options) {
    /** настройки */
    this.$options = new Options(options);

    /** элемент контейнера */
    this.$elem = null;
    /** карта */
    this.$map = null;
    /** маркер */
    this.$marker = null;
    /**
     * событие клика
     * @param {number} building идентификатор здания
     * @param {Array} location местоположение
     */
    this.onclick = function (building, location) {
    };
  }

  /**
   * смонтировать карту
   * @param {string} selector селектор
   */
  mount(selector) {
    const _this = this;

    const options = this.$options;

    const $elem = selector instanceof Element ? selector : document.querySelector(selector);
    if ($elem) {
      this.$elem = $elem;
    } else {
      console.error('Элемент не найден: ' || selector);
    }

    // создаем слой фич
    const vectorSource = this.$vectorSource = new VectorSource({
      format: new GeoJSON(),
      loader: (e, r, p) => LayerLoader(this, vectorSource, e, r, p),
      strategy: bbox
    });

    const styleFunction = function (feature) {
      return options.styles[feature.getGeometry().getType()];
    };

    const vectorLayer = new VectorLayer({
      source: vectorSource,
      style: styleFunction
    });

    // создаем объект карты
    const map = this.$map = new Map({
      target: $elem,
      layers: [
        new TileLayer({
          source: new OSM()
        }),
        vectorLayer
      ],
      view: new View({
        projection: 'EPSG:900913',
        extent: transformExtent([51.75, 56.05, 59.5, 61.72], 'EPSG:4326', 'EPSG:900913'),
        center: fromLonLat(options.map.center),
        minZoom: options.map.minZoom,
        maxZoom: options.map.maxZoom,
        zoom: options.map.zoom
      })
    });

    // обработчики событий
    if (options.mode == 'location') {
      map.on('click', function (e) {
        let buildingID = null;
        let location = transform(e.coordinate, 'EPSG:900913', 'EPSG:4326');
        let $feature = null;

        map.forEachFeatureAtPixel(e.pixel, function (feature, layer) {
          const properties = feature.getProperties();
          if (properties.buildingID) buildingID = properties.buildingID;
          if (properties.location) location = properties.location;
          $feature = feature;
        });

        // вызываем событие клика
        _this.onclick(buildingID, location, $feature);

        // создаем маркер
        if (location) {
          let marker = _this.$marker;
          if (!marker) {
            // создаем маркер
            marker = _this.$marker = new Feature({
              geometry: new Point(transform(location, 'EPSG:4326', 'EPSG:900913')),
              name: 'Вы здесь',
            });
            marker.setStyle(options.styles['Marker']);
            vectorSource.addFeature(marker);
          } else {
            // перемещаем маркер
            marker.getGeometry().setCoordinates(transform(location, 'EPSG:4326', 'EPSG:900913'));
          }
        }
      });
    }
  }

  /**
   * обновить аналитику
   * @param {number} serviceID
   * @param {Date} start
   * @param {Date} end
   */
  computeAnalytics(serviceID, start, end) {
    const startDate = `${start.getFullYear()}/${start.getMonth()}/${start.getDate()}`;
    const endDate = `${end.getFullYear()}/${end.getMonth()}/${end.getDate()}`;

    const url = `${BASE_URL}Map/GetAnalytics?serviceID=${serviceID}&start=${startDate}&end=${endDate}`;
    // http://doomer/Urban/api/Map/GetAnalytics?serviceID=8&start=2019/06/16&end=2019/06/23

    const _this = this;

    axios.get(url)
      .then((res) => {
        // сохраняем аналитику
        _this.$analytics = res.data;
        _this.updateFeatureAnalytics();
      })
      .catch((err) => {
        throw new Error(err);
      });
  }

  /** обновить фичи */
  updateFeatureAnalytics() {
    const vectorSource = this.$vectorSource;
    if (!vectorSource) return;

    const analytics = this.$analytics;
    const options = this.$options;

    if (analytics) {
      for (let item of analytics) {
        const feature = vectorSource.getFeatureById(item.ID);
        if (feature) {
          const value = item.Value;
          let risk;
          if (value >= 0.8) risk = 5;
          else if (value >= 0.6) risk = 4;
          else if (value >= 0.4) risk = 3;
          else if (value >= 0.2) risk = 2;
          else risk = 1;
          feature.setStyle(options.styles['Risk' + risk.toString()]);
        }
      }
    }
  }
}

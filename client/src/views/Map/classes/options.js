import { Circle as CircleStyle, Icon, Fill, Stroke, Style } from 'ol/style.js';

import { extend } from '../helpers/utils';

const riskOpacity = 1;

const riskColor1 = `rgba(132, 201, 64, ${riskOpacity})`
const riskColor2 = `rgba(252, 196, 51, ${riskOpacity})`
const riskColor3 = `rgba(252, 160, 51, ${riskOpacity})`
const riskColor4 = `rgba(249, 111, 34, ${riskOpacity})`
const riskColor5 = `rgba(212, 68, 49, ${riskOpacity})`

/**
 * настройки системы
 */
export default class Options {
  /**
   * конструктор
   * @param {Object} options настройки
   */
  constructor(options) {
    /** настройки карты */
    this.map = {
      /** центр карты */
      center: [56.250, 58.000],
      /** уровень приближения по умолчанию */
      zoom: 16,
      /** минимальный уровень приближения */
      minZoom: 15,
      /** максимальный уровень приближения */
      maxZoom: 18,
    };

    /** режим работы карты */
    this.mode = 'location';

    /** стили */
    this.styles = {
      'Marker': new Style({
        image: new Icon(({
          anchor: [0.5, 1],
          anchorXUnits: 'fraction',
          anchorYUnits: 'fraction',
          src: '/marker.png'
        }))
      }),
      'Point': new Style({
        //image: image
      }),
      'LineString': new Style({
        stroke: new Stroke({
          color: 'green',
          width: 1
        })
      }),
      'MultiLineString': new Style({
        stroke: new Stroke({
          color: 'green',
          width: 1
        })
      }),
      'MultiPoint': new Style({
        //image: image
      }),
      'MultiPolygon': new Style({
        stroke: new Stroke({
          color: 'blue',
          width: 1
        }),
        fill: new Fill({
          color: 'rgba(0, 0, 255, 0.2)'
        })
      }),
      'Polygon': new Style({
        //stroke: new Stroke({
        //  color: 'orangered',
        //  width: 1
        //}),
        fill: new Fill({
          color: 'rgba(255, 69, 0, 0.1)'
        })
      }),
      'Risk1': new Style({
        stroke: new Stroke({
          color: riskColor1,
          width: 1
        }),
        fill: new Fill({
          color: riskColor1
        })
      }),
      'Risk2': new Style({
        stroke: new Stroke({
          color: riskColor2,
          width: 1
        }),
        fill: new Fill({
          color: riskColor2
        })
      }),
      'Risk3': new Style({
        stroke: new Stroke({
          color: riskColor3,
          width: 1
        }),
        fill: new Fill({
          color: riskColor3
        })
      }),
      'Risk4': new Style({
        stroke: new Stroke({
          color: riskColor4,
          width: 1
        }),
        fill: new Fill({
          color: riskColor4
        })
      }),
      'Risk5': new Style({
        stroke: new Stroke({
          color: riskColor5,
          width: 1
        }),
        fill: new Fill({
          color: riskColor5
        })
      }),
      'GeometryCollection': new Style({
        stroke: new Stroke({
          color: 'magenta',
          width: 2
        }),
        fill: new Fill({
          color: 'magenta'
        }),
        image: new CircleStyle({
          radius: 10,
          fill: null,
          stroke: new Stroke({
            color: 'magenta'
          })
        })
      }),
      'Circle': new Style({
        stroke: new Stroke({
          color: 'red',
          width: 2
        }),
        fill: new Fill({
          color: 'rgba(255,0,0,0.2)'
        })
      })
    };

    // расширяем настройки
    extend(true, this, options);
  }
}

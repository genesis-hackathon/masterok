﻿using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

using Newtonsoft.Json;

namespace Urban.DataService
{
    /// <summary>
    /// обработчик ошибок
    /// </summary>
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;

        /// <summary>
        /// конструктор
        /// </summary>
        /// <param name="next"> следующий делегат в конвеере </param>
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            HttpStatusCode code;

            if (exception is ServiceException ex)
            {
                // исключение сервиса
                code = ex.Code;
            }
            else
            {
                // ошибка 500, если не указано иначе
                code = HttpStatusCode.InternalServerError;
            }

            var result = JsonConvert.SerializeObject(new
            {
                exception.Message,
                exception.StackTrace,
            }, Formatting.Indented);
            context.Response.ContentType = "application/json; charset=utf-8";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result, Encoding.UTF8);
        }
    }
}

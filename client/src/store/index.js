import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        services: [],
        requests: []
    },
    mutations: {
        setServices(state, payload) {
            state.services = payload;
        },
        
        setRequests(state, payload) {
            state.requests = payload;
        },

        changeRequest(state, payload) {
            // const request = state.find(e => e.ID === payload.id);
            state.requests = state.requests.map(e => {
                if (e.ID !== payload.ID) return e;

                return {
                    ...e,
                    ...payload
                };
            });
        }
    },
    getters: {
        request: ({ requests }) => id => requests.find(e => e.ID === id)
    },
    
    actions: {}
});

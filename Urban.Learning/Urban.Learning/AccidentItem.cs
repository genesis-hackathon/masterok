﻿using System;

namespace Urban.Learning
{
    /// <summary>
    /// авария
    /// </summary>
    public class AccidentItem
    {
        /// <summary>
        /// дата
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// причина нарушения
        /// </summary>
        public int Reason { get; set; }

        /// <summary>
        /// идентификатор здания
        /// </summary>
        public int Building { get; set; }
    }
}